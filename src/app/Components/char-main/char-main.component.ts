import {Component, ElementRef, OnInit} from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-char-main',
  templateUrl: './char-main.component.html',
  styleUrls: ['./char-main.component.scss']
})
export class CharMainComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
    this.createChart()
  }

  public createChart() {

    const svgRect = d3.select('#rect').append('svg')
    const svgCircle = d3.select('#circle').append('svg')

    
    svgRect.append('rect')
      .attr('width', 80) // Ширина квадрата
      .attr('height', 80) // Высота квадрата
      .attr('fill', 'red')
    // Цвет квадрата

    svgCircle.append('circle')
      .attr('cx', 50) // координата X центра круга
      .attr('cy', 50) // координата Y центра круга
      .attr('r', 50)   // радиус круга
      .attr('fill', 'blue'); // цвет заливки

  }
}
