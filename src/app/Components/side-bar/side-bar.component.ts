import {Component, OnInit} from '@angular/core'
import {IdataBar} from '../../model/dataBar'
import {DataService} from '../../service/data.service'

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})

export class SideBarComponent implements OnInit {

  constructor(public dataService: DataService) {
  }

  public items: IdataBar[] = this.dataService.myList


  ngOnInit(): void {
  }

}
