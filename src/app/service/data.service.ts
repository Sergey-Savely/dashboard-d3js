import { Injectable } from '@angular/core';
import {IdataBar} from '../model/dataBar';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  myList: IdataBar[] = [
    { name: 'order' },
    { name: 'products' },
    { name: 'sales-report' },
    { name: 'messages' },
    { name: 'settings' },
    { name: 'sign-out' },
  ]

}
